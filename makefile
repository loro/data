clean:
	find . -name '*.sw*' -delete
	rm models/*.js
	rm spec/*.js
	rm routes/*.js
	rm app/*.js

test:
	tsc spec/*.ts --module commonjs
	jasmine-node spec/
