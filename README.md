# loro/data
## loro's web services

## Install
```
Install Heroku toolbelt from https://toolbelt.heroku.com/
$ git clone git@gitlab.com:loro/data.git
$ cd data/
$ npm install
$ heroku login
```
## Build
To build files 
```
$ tsc [filename.ts] --module commonjs
```
To build and deploy project to `localhost:3000`
```
$ ts-node data/bin/www
```
## Test
To test web services
```
$ jasmine-node data/spec/
```
## Deploy
To deploy to Heroku
```
$ git push heroku master
```