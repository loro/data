///<reference path="../typings/tsd.d.ts"/>
///<reference path="../models/loro.ts"/>

import * as express from 'express';
import * as mongoose from 'mongoose';
import * as request from 'request';

import {ILoro, ILoroModel, Loro} from '../models/loro';

var router: express.Router  = express.Router();
mongoose.connect("mongodb://heroku_3mz2bqfm:rrir055vsvjlip7362048ekmtj@ds037195.mongolab.com:37195/heroku_3mz2bqfm");

const clientID = "99ba5c561e534d3db1adecd1da6989ce"
const clientSecret = "1894a91c048542cb89fca3b36d109706";
const callbackURL = "spotifylororadio://returnafterlogin";

// check routes working
router.get('/', function (req, res, next) {
  res.render('api', { title: 'api' });
});

router.post('/auth/swap/:code', function (req, res, next) { 
  request.post({
    url: "https://accounts.spotify.com/api/token",
    form: {
      grant_type: "authorization_code",
      redirect_uri: callbackURL,
      code: req.params.code 
    }
  },
  function (error, response, body) {
    if (error)
      return next(error);

    res.status(response.statusCode).json(body); 
  });
});

router.post('/auth/refresh/:refresh_token', function (req, res, next) {
  request.post({
    url: "https://accounts.spotify.com/api/token",
    form: {
      grant_type: "refresh_token",
      refresh_token: req.params.refresh_token
    }
  },
  function (error, response, body) {
    if (error)
      return next(error);

    res.status(response.statusCode).json(body); 
  });
})

// POST create new loro
// GET  get loros
router
  .route('/loro')
  .post(function (req, res, next) {
    var loro: ILoroModel = new Loro({
      name: req.body.name,
      is_private: req.body.is_private,
      is_collaborative: req.body.is_collaborative,
      is_live: req.body.is_live,
      city: req.body.city,
      state: req.body.state,
      date_created: Date.now()
    });

    if (req.body.longitude && req.body.latitude)
    {
      loro.set('coordinates', [req.body.longitude, req.body.latitude], [Number]);
    }

    loro.save(function (error, loro) {
      if (error)
        return next(error);

      res.status(201).json(loro);
    });
  })
  .get(function (req, res, next) {
    Loro
      .find({ is_private: false })
      .sort({ date_created: 'descending' })
      .lean()
      .exec(function (error, loros) {
        if (error)
          return next(error);

        if (!loros)
          res.sendStatus(204);
        else
          res.status(200).json(loros);
    });
  });

router
  .route('/loro/coordinates/')
  .get(function(req, res, next) {
    Loro.geoNear({
      type: "Point",
      coordinates: [parseFloat(req.query.longitude), parseFloat(req.query.latitude)]
    },
    {
      maxDistance: 80000,
      spherical: true,
      query: { is_private: false }
    },
    function(error, loros, stats) {
      if (error)
        return next(error);

      if (!loros)
        res.sendStatus(204);
      else
        res.status(200).json(loros); 
    });
  });

router
  .route('/loro/region/')
  .get(function(req, res, next) {
    Loro.find({
      'city': req.query.city,
      'state': req.query.state
    }).lean()
      .exec(function(error, loros: ILoro[]) {
        if (error)
          return next(error);

        res.status(200).json(loros);
      });
  });

// GET    get a loro
// DELETE delete a loro
router
  .route('/loro/:loro_id')
  .get(function(req, res, next) {
    Loro
      .findById(req.params.loro_id)
      .lean()
      .exec(function (error, loro: ILoro) {
        if (error)
          return next(error);

        if (!loro)
          res.sendStatus(404);
        else
          res.json(loro);
      });
  })
  .delete(function(req, res, next) {
    Loro.findByIdAndRemove(req.params.loro_id, function (error) {
      if (error)
        return next(error);

      res.sendStatus(204);
    });
  });

export = router;
