/// <reference path="../typings/tsd.d.ts"/>

import * as express from 'express';

var router: express.Router = express.Router();

/* GET client page. */
router.get('/', function(req: express.Request,
                         res: express.Response, 
                         next: Function) {
  res.render('client', { title: 'client', count: 0 });
});

export = router;
