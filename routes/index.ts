/// <reference path="../typings/tsd.d.ts"/>

import * as express from 'express';

var router: express.Router = express.Router();

router.get('/', function(req: express.Request,
                         res: express.Response,
                         next: Function) {
  res.render('index', { title: 'loro' });
});

export = router;
