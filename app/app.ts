/// <reference path="../typings/tsd.d.ts"/>

import * as express from  'express';
import * as index from  '../routes/index';
import * as api from '../routes/api';
import * as client from '../routes/client';
import * as path from 'path';

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname,
                          '../public/images',
                          'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/api', api);
app.use('/client', client);

// catch 404 and forward to error handler
var requestHandler: express.RequestHandler;
requestHandler = function(req: express.Request,
                          res: express.Response,
                          next: Function) {
  var err: any = new Error('Not Found');
  err.status = 404;
  next(err);
}
app.use(requestHandler);

// error handlers

// development error handler
// will print stacktrace
var developHandler: express.ErrorRequestHandler;
developHandler = function(err: any,
                          req: express.Request,
                          res: express.Response,
                          next: Function) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: err
    });
}
if (app.get('env') === 'development') {
  app.use(developHandler);
}

// production error handler
// no stacktraces leaked to user
var productionHandler: express.ErrorRequestHandler;
productionHandler = function(err: any,
                             req: express.Request,
                             res: express.Response,
                             next: Function) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
}

app.use(productionHandler);

export = app;
