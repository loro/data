///<reference path="../typings/tsd.d.ts"/>
///<reference path="../models/loro.ts"/>

import * as server from 'socket.io';
import * as mongoose from 'mongoose';
import * as chalk from 'chalk';

import {ILoro, ILoroModel, Loro} from '../models/loro';
import {ITrack} from '../models/track.ts'

var assert = require('assert');

interface TrackRequest {
  track_uri: string;
}
interface TrackNowPlaying {
  track: any;
  began_playing_at: Date 
}

mongoose.createConnection("mongodb://heroku_3mz2bqfm:rrir055vsvjlip7362048ekmtj@ds037195.mongolab.com:37195/heroku_3mz2bqfm");

var io: SocketIO.Server = server();
var lobby_count: number = 0;

io.on('connection', function (socket: SocketIO.Socket) {
  io.emit('update', { count: ++lobby_count });
  socket.on('disconnect', function () { lobby_count--; });

  socket.on('join_request', function(loro_id: string) {
    Loro.findById(loro_id)
        .lean()
        .exec(function(error, found_loro: ILoro) {

          console.log('-- recieved ', 'join_request');

          if (error || !found_loro) {
            socket.emit('join_fail');

            console.log('--- emitted ', chalk.red('join_fail'));
            if (error) {
              console.log(error);
            }
          }
          else {
            socket.leaveAll();
            socket.join(loro_id);
            socket.emit('join_success');

            console.log('--- emitted ', chalk.green('join_success'));
          }
        });
  });

  socket.on('add_track_request', 
    function(loro_id: string, track_request: TrackRequest) {
      Loro.findByIdAndUpdate(loro_id, 
      {
        $push: {
          play_list: { track_uri: track_request.track_uri }
        } 
      },
      {
        select: 'next_track play_list',  
        new: true
      },
      function(error, found_loro: ILoroModel) {

        console.log('-- recieved ', 'add_track_request');
        console.log('--- with ', track_request); 

        if (error || !found_loro) {
          socket.emit('add_track_fail');

          console.log('--- emitted ', chalk.red('add_track_fail'));
          if (error) {
            console.log(error);
          }
        }
        else {
          socket.emit('add_track_success');

          console.log('--- emitted ', chalk.green('add_track_success'));
          
          if (!found_loro.next_track && 
               found_loro.play_list.length) { 

            var next_track: ITrack = found_loro.play_list.shift();
            assert(next_track);

            found_loro.set('next_track', next_track);
            found_loro.save(function (error) {
              if (error) {
                console.log('--- emitted ', chalk.red('next_track_update_fail'));
                console.log(error);
              }
              else {
                io.to(loro_id).emit('next_track_updated', {
                  track_uri: next_track.track_uri
                });

                console.log('--- emitted ', chalk.green('next_track_updated'));
                console.log('---- with ', next_track);
              }
            });
          }
        }
      });
  });

  socket.on('now_playing_update_request', 
    function(loro_id: string, now_playing_update: TrackNowPlaying) {
      Loro.findByIdAndUpdate(loro_id, 
        {
          $set: {
            now_playing: {
              track: now_playing_update.track,
              began_playing_at: now_playing_update.began_playing_at 
            },
          }
        }, 
        { 
          select: 'now_playing next_track play_list',
          new: true
        }, 
        function(error, found_loro: ILoroModel) {
          
          console.log('-- recieved ', 'now_playing_update_request');
          console.log('--- with ', now_playing_update); 

          if (error || !found_loro || !found_loro.now_playing) {
            socket.emit('now_playing_update_fail');

            console.log('--- emitted ', chalk.red('now_playing_update_fail'));
            if (error) {
              console.log(error);
            }
          }
          else {
            socket.emit('now_playing_update_success');

            console.log('--- emitted ', chalk.green('now_playing_update_success'));
            if (!found_loro.play_list.length) { // play_list empty
              socket.emit('play_list_empty');

              console.log('--- emitted ', chalk.red(' play_list_empty'));
            }
            else {
              var next_track: ITrack = found_loro.play_list.shift();

              // throw exception and add handler
              assert(next_track);

              found_loro.set('next_track', next_track);
              found_loro.save(function (error) {
                if (error) {
                  console.log(error);
                }
                else {
                  io.to(loro_id).emit('next_track_updated', {
                    track_uri: next_track.track_uri
                  });

                  console.log('--- emitted ', chalk.green('next_track_updated'));
                  console.log('---- with', next_track);
                }
              });
            }
          }
      });
  });

  socket.on('next_track_request', function(loro_id: string) {
    Loro.findById(loro_id)
        .where('next_track')
        .exists(true)
        .select('next_track')
        .lean()
        .exec(function(error, found_loro: ILoro) {
          console.log('-- recieved', 'next_track_request');

          if (error || !found_loro) {
            socket.emit('next_track_fail');
            console.log('--- emmitted ', chalk.red('next_track_fail'));
            if (error) {
              console.log(error);
            }
          }
          else {
            var next_track: TrackRequest = {
              track_uri: found_loro.next_track.track_uri
            }
            socket.emit('next_track_success', next_track);

            console.log('--- emitted ', chalk.green('next_track_success'));
            console.log('---- with', next_track);
          }
        });
  });

  socket.on('now_playing_request', function(loro_id: string) {
    Loro.findById(loro_id)
        .where('now_playing')
        .exists(true)  
        .select('play_list now_playing')
        .lean()
        .exec(function(error, found_loro: ILoro) {
          console.log('-- recieved', 'now_playing_request');

          if (error || !found_loro) {
            socket.emit('now_playing_fail');
            console.log('--- emmitted ', chalk.red('now_playing_fail'));
            if (error) {
              console.log(error);
            }
          }
          else {
            var now_playing: TrackNowPlaying = {
              track: found_loro.now_playing.track,
              began_playing_at: found_loro.now_playing.began_playing_at
            };
            socket.emit('now_playing_success', now_playing); 

            console.log('--- emitted ', chalk.green('now_playing_success'));
            console.log('---- with', now_playing);
          }
        });
    });

  socket.on('next_track_vote_up', function(loro_id: string) {
    Loro.findByIdAndUpdate(loro_id, { 
      $inc: { 
        'next_track.votes': 1 
      } 
    }).where('next_track')
      .exists(true) 
      .exec(function(error, found_loro: ILoroModel) {
        console.log('-- recieved ', 'next_track_vote_up');

        if (error || !found_loro) {
          socket.emit('next_track_vote_fail');
          console.log('--- emitted ', chalk.red('next_track_vote_fail'));
          if (error)
            console.log(error);
        }
        else {
          socket.emit('next_track_vote_success');
          console.log('--- emitted ', chalk.green('next_track_vote_success'));
        }
      });
  });

  socket.on('next_track_vote_down', function(loro_id: string) {
    Loro.findByIdAndUpdate(loro_id, { 
      $inc: { 
        'next_track.votes': -1 
      } 
    }, { 
      select: 'next_track play_list',
      new: true
    }).where('next_track')
      .exists(true)
      .exec(function(error, found_loro: ILoroModel) {
        console.log('-- recieved ', 'next_track_vote_down');

        if (error || !found_loro) {
          console.log(error);
          console.log('--- emitted ', chalk.red('next_track_vote_fail'));
        }
        else {
          socket.emit('next_track_vote_success');
          console.log('--- emitted ', chalk.green('next_track_vote_success'));

          if (found_loro.next_track.votes <= -5 && found_loro.play_list) {

            var next_track: ITrack = found_loro.play_list.shift(); 

            assert(next_track);

            found_loro.set('next_track', next_track);
            found_loro.save(function (error) {
              if (error) {
                console.log(error);
              }
              else {
                io.to(loro_id).emit('next_track_updated', { 
                  track_uri: next_track.track_uri
                });

                console.log('--- emitted ', chalk.green('next_track_updated'));
                console.log('---- with', next_track);
              }
            });
          }
        }
      });
  });
});

export = io;
