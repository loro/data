export class TrackBank {
  public bank: string[];
  constructor() {
    this.bank = [ 
      "spotify:track:3FtYbEfBqAlGO46NUDQSAt",
      "spotify:track:0mBkoM8r7KAQzZij5swTUL",
      "spotify:track:48B0xxpJuXiYFwDHRYaIt3",
      "spotify:track:4H4KkHfXJs3cQEnbNW3bVS",
      "spotify:track:2cHDE9RaJhcVRhYMD0dtiQ",
      "spotify:track:3F5tG1RxiT9EPCtPEdUodD",
      "spotify:track:7drqcBBZwddPvFkHbvnCeM",
      "spotify:track:2cuMdct1864EEczZgN9nYg",
      "spotify:track:7MVRbVf3CWqypnIBfDvTg8",
      "spotify:track:3720k9XGitS6jDcBi1muK7",
      "spotify:track:6t5syYlCH51Gje6CV4IZZp",
      "spotify:track:4e6QAT8kC0lILyA8dTLSvg",
      "spotify:track:1BIZXYxk5ACcNibwCH5MXw",
      "spotify:track:1j6kDJttn6wbVyMaM42Nxm"
    ];
  }

  randomTrack() {
    return this.bank[Math.floor(Math.random() * this.bank.length)];
  }
}

