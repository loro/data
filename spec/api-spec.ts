#!javascript

///<reference path="../typings/tsd.d.ts"/>
///<reference path="../models/loro.ts"/>
///<reference path="./track_bank.ts"/>

import * as request from 'request';
import * as io from 'socket.io-client';
import * as chalk from 'chalk';
import {ILoroModel} from '../models/loro';
import {TrackBank} from './track_bank';

var url = process.env.REMOTE? 'http://loro-music.herokuapp.com' : 'http://localhost:3000';
var endpoint = url + '/api/loro';
var client = io(url);
var trackBank = new TrackBank();

describe("tests loro's REST API", function() {
  it("should POST new loro to /loro", function(done) {
    var words = ["drink", "sleep", "code", "eat"];

    var shuffle = function (o: string[]): string[] {
      for (var j, x, i = o.length; i; 
            j = Math.floor(Math.random() * i), 
            x = o[--i], 
            o[i] = o[j], 
            o[j] = x);

      return o;
    }

    var name = shuffle(words).join(', '); 

    console.log('\n-- sent ', 'POST');
    console.log('--- to ', endpoint);
    console.log('--- with ', name);
    request.post({
      url: endpoint,
      form: {
        name: name,
        is_private: false,
        is_collaborative: true,
        is_live: false,
        city: "Miami",
        state: "Florida",
        longitude: -80.4582,
        latitude: 25.5584
      }
    }, 
    function(error, response, body) {
      expect(response.statusCode).toBe(201);
      var loro: ILoroModel = JSON.parse(body);
      
      console.log('--- recieved ', response.statusCode);
      console.log('---- with');
      console.log(loro);

      client.emit('now_playing_update_request', loro._id, {
        track: { track_uri: trackBank.randomTrack() },
        began_playing_at: Date.now()
      });

      if (error)
        console.log(error);
      done();
    });
  });

  it("should GET all public loros from /loro", function(done) {
    console.log('\n--sent ', 'GET');
    console.log('--- to ', endpoint);
    request(endpoint, function(error, response, body) {
      expect(response.statusCode).toBe(200);
      console.log('--- recieved ', response.statusCode);
      if (error)
        console.log(error);
      done();
    });
  });
});
