#!javascript

///<reference path="../typings/tsd.d.ts"/>
///<reference path="../models/loro.ts"/>
///<reference path="../models/track.ts"/>
///<reference path="./track_bank.ts"/>

import {ILoroModel} from '../models/loro';
import {ITrack} from '../models/track';
import {TrackBank} from './track_bank';
import * as io from 'socket.io-client';
import * as request from 'request';
import * as assert from 'assert';
import * as chalk from 'chalk';

var url = process.env.REMOTE? 'http://loro-music.herokuapp.com':'http://localhost:3000';
var endpoint = url + '/api/loro';
var trackBank = new TrackBank();

var randomItem = function(array) {
  return array[Math.floor(Math.random() * array.length)];
}

interface Test {
  track: ITrack;
  loro: ILoroModel;
}

describe("socket this.client.test", function() {
  var test: Test = {
    track: { track_uri: trackBank.randomTrack() },
    loro: null 
  }; 

  beforeEach(function(done) {
    this.client = io(url);
    console.log('\n-- sent GET');
    console.log('--- to ', endpoint);
    request(endpoint, function(err, response, body) {
      expect(response.statusCode).toBe(200);   
      console.log('--- recieved', response.statusCode);

      test.loro = randomItem(JSON.parse(body));
      test.track.track_uri = trackBank.randomTrack();

      expect(test.loro).not.toBeNull();
      console.log('--- testing loro', test.loro._id);
      console.log('--- with track', test.track);
      
      done();
    }); 
  });

  it("shoud recieve join_success with valid id", function(done) {
    this.client.emit("join_request", test.loro._id);

    console.log('-- sent join_request');
    console.log('--- to ', test.loro._id);

    this.client.on("join_success", function() { 
      console.log('--- recieved ', chalk.green('join_success'));
      done(); 
    });
    this.client.on('join_fail', function() {
      console.log('--- recieved ', chalk.red('join_fail'));
      done();
    })
  });

  it("should push track to playlist", function(done) {
    assert(test.track);
    this.client.emit('add_track_request', test.loro._id, test.track);

    console.log('-- sent add_track_request');
    console.log('--- to ', test.loro._id); 
    console.log('--- with ', test.track);

    this.client.on('add_track_success', function() {
      console.log('--- recieved ', chalk.green('add_track_success'));
      done();
    });
    this.client.on('add_track_fail', function() {
      console.log('--- recieved ', chalk.red('add_track_fail'));
      done();
    })
  });

  it("should update now playing", function(done) {
    assert(test.track);
    this.client.emit('now_playing_update_request', test.loro._id, {
      track: test.track,
      began_playing_at: Date.now()
    });

    console.log('-- sent now_playing_update_request'); 
    console.log('--- to', test.loro._id);
    console.log('--- with ', test.track); 

    this.client.on('now_playing_update_success', function() {
      console.log('--- recieved ', chalk.green('now_playing_update_success'));
      done();
    });
    this.client.on('now_playing_update_fail', function() {
      console.log('--- recieved ', chalk.red('now_playing_update_fail'));
      done();  
    });
  });

  it("should get next track uri", function(done) {
    this.client.emit('next_track_request', test.loro._id);

    console.log('-- sent next_track_request');
    console.log('--- to ', test.loro._id); 

    this.client.on('next_track_success', function(next_track) {
      console.log('--- recieved ', chalk.green('next_track_success'));
      console.log('---- with', next_track);

      expect(test.loro.next_track).not.toBeUndefined();
      expect(next_track).not.toBeNull(); 

      if (Math.floor(Math.random() * 2)) {
        console.log('---- sent next_track_vote_up');
        this.client.emit('next_track_vote_up', test.loro._id);
      }
      else {
        console.log('---- sent next_track_vote_down');
        this.client.emit('next_track_vote_down', test.loro._id);
      }

      done();
    });
    this.client.on('next_track_fail', function() {
      console.log('--- recieved ', chalk.red('next_track_fail'));
      expect(test.loro.next_track).toBeUndefined();
      done();
    })
  });

  it("should get now playing uri", function(done) {
    this.client.emit('now_playing_request', test.loro._id);

    console.log('-- sent now_playing_request');
    console.log('--- to', test.loro._id); 

    this.client.on('now_playing_success', function(now_playing) {
      console.log('--- recieved ', chalk.green('now_playing_success'));
      console.log('---- with', now_playing);

      expect(test.loro.now_playing).not.toBeUndefined();
      expect(now_playing.track_uri).not.toBeNull(); 
      expect(now_playing.began_playing_at).not.toBeNull(); 

      done();
    });
    this.client.on('now_playing_fail', function() {
      console.log('--- recieved ', chalk.red('now_playing_fail'));
      expect(test.loro.now_playing).toBeUndefined();
      done();
    });
  }); 
});
