///<reference path="../typings/tsd.d.ts"/>
///<reference path="./track.ts"/>

import * as mongoose from 'mongoose';
import {TrackSchema, ITrack} from './track';

export interface ILoro {
  name: string;
  is_private: boolean;
  is_collaborative: boolean;
  is_live: boolean;
  date_created: Date;
  now_playing: {
    track: ITrack,
    began_playing_at: Date
  };
  next_track: ITrack;
  play_list: [ITrack];
  coordinates: number[],
  city: string,
  state: string
}

export var LoroSchema = new mongoose.Schema({
  name: { type: String, required: true },
  is_private: { type: Boolean, required: true },
  is_collaborative: { type: Boolean, required: true },
  is_live: { type: Boolean, required: true },
  date_created: { type: Date, default: Date.now },
  now_playing: {
    track: { type: TrackSchema },
    began_playing_at: { type: Date }
  },
  next_track: TrackSchema,
  play_list: [TrackSchema],
  coordinates: { type: [Number], index: '2dsphere' },
  city: String,
  state: String
});

export interface ILoroModel extends ILoro, mongoose.Document {}

export var Loro = mongoose.model<ILoroModel>('Loro', LoroSchema);
