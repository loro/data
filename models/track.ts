///<reference path="../typings/tsd.d.ts"/>

import * as mongoose from 'mongoose';

export interface ITrack {
  track_uri: string,
  votes?: number
}

export var TrackSchema = new mongoose.Schema({
  track_uri: { type: String, required: true },
  votes: { type: Number, default: 0 }
});
